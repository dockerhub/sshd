SHELL = /bin/bash

build:
	@docker build -t dockerhub/sshd .

push: build
	@$(eval REV := $(shell git rev-parse HEAD|cut -c 1-8))
	@docker tag dockerhub/sshd registry.gitlab.com/dockerhub/sshd
	@docker push registry.gitlab.com/dockerhub/sshd

